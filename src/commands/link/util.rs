use std::os::unix::fs::symlink;
use std::path::Path;

// Handles the logic of making sure a symlink happens.
// 
// In the case of pre-existing files we first make sure the user is okay with us deleting their
// existing files.  
pub fn process_file(from: &Path, to: &Path, actually_link: bool) {
    // If not actually linking, print the link we would have generated and exit
    if !actually_link {
        println!("Would symlink: {} -> {}", from.display(), to.display());
        return;
    }

    // If the destination exists, confirm before updating
    if to.exists() {
        if request_overwrite(to.to_str().unwrap()) {
            remove_file(&to);
        } else {
            return;
        }
    }

    match symlink(from, to) {
        Ok(_) => println!("Symlink: {} -> {}", from.display(), to.display()),
        Err(_) => eprintln!("Symlink failed: {} -> {}", from.display(), to.display())
    };
}

pub fn remove_file(path: &Path) {
    // Use symlink_metadata() to make sure we don't follow any symlinks back to their original
    // data. This allows us to detect whether overwritten files are symlinks or not, as the
    // process to remove them is subtly different for each type (symlink, file, directory).
    let metadata = path.symlink_metadata().expect("Should have been able to get metadata");

    if metadata.is_dir() {
        // Path is a directory, which may or may not be empty.
        // We call remove_dir_all because the user has already told us we can delete it, so we
        // assume that they've checked the directory out already and are fine losing its
        // contents.
        std::fs::remove_dir_all(path)
            .expect("Expected to be able to remove pre-existing directory");
    } else if metadata.is_file() {
        // Path is a plain file, so just remove it.
        std::fs::remove_file(path)
            .expect("Expected to be able to remove pre-existing file");
    } else {
        // Path points to a symlink, we can use remove_file here as well.
        std::fs::remove_file(path)
            .expect("Expected to be able to remove symlink");
    }
}

// Gets permission from standard input on whether or not to overwrite an existing FS path with
// our symlink.
pub fn request_overwrite(file_path: &str) -> bool {
    let yes_answers = vec![ "y", "Y" ];
    let no_answers = vec![ "n", "N" ];
    let confirmation_message = format!("File exists: {}. Overwrite? [y/n] ", file_path);
    loop {
        let response = input(&confirmation_message);
        let response_trimmed = response.trim();
        if yes_answers.contains(&response_trimmed) {
            return true;
        } else if no_answers.contains(&response_trimmed) {
            return false;
        }
    }
}

// Get's user input with a message in a way that helps keep the lines consistent if we have to
// re-loop.
pub fn input(prompt: &str) -> String {
    let mut user_input = String::new();
    print!("{}", prompt); // Print the prompt

    std::io::Write::flush(&mut std::io::stdout()) // Flush stdout
        .expect("Stdout flush failed while taking user input");

    std::io::stdin().read_line(&mut user_input) // Accept user input
        .expect("Taking user input from Stdin failed"); 

    user_input.pop(); // Remove the newline character
    return user_input;
}

