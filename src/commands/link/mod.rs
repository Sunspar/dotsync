pub(crate) mod util;

use clap::Clap;
use serde::Deserialize;
use serde_yaml;
use std::path::PathBuf;

#[derive(Clap, Debug)]
pub struct LinkArguments {
    /// The path to read symlink options from.
    #[clap(short='f', long="config-file", default_value="dotsync-links.yml")]
    pub config_file: String
}

#[derive(Deserialize, Debug)]
struct ConfigFile {
    /// Whether to dry-run the symlinks or not.
    pub dry_run: bool,

    /// The set of symlinks to manage.
    pub links: Vec<Link>
}

#[derive(Deserialize, Debug)]
struct Link {
    /// The symlink's initial starting point (where to create the symlink itself).
    pub from: String,

    /// The symlink's reference (where to point the symlink to).
    pub to: String
}

pub fn run(args: &LinkArguments) {
    // Load the config file
    let config_file = std::fs::read_to_string(&args.config_file)
        .expect("Failed to read config file");
  
    let config = serde_yaml::from_str::<ConfigFile>(&config_file)
        .expect("Failed to parse config file.");

    // Cache out the current directory as a small optimziation
    let current_dir = std::env::current_dir().expect("Cant get current dir.");

    //  Process all the files to link.
    for item in config.links {
        // Build out the destination path for the symlink
        let mut dest_path_buf = PathBuf::new();
        dest_path_buf.push(item.to);
        let dest_path = dest_path_buf.as_path();

        // Build out the source path for the symlink
        let mut src_path_buf = PathBuf::new();
        src_path_buf.push(&current_dir);
        src_path_buf.push(&item.from);
        let src_path = src_path_buf.as_path();

        // link the file
        util::process_file(src_path, dest_path, !config.dry_run);
    }
}

