mod commands;

use clap::Clap;
use commands::link::LinkArguments;

#[derive(Clap, Debug)]
#[clap(version="0.1", author="Sunspar")]
struct AppArguments {
    /// The subcommand being invoked.
    #[clap(subcommand)]
    pub command: Command
}

#[derive(Clap, Debug)]
enum Command {
    /// Manages symlinks.
    #[clap(name = "link")]
    Link(LinkArguments)
}

fn main() {
    let opts = AppArguments::parse();
    
    match opts.command {
        Command::Link(ref args) => commands::link::run(args)
    }
}
